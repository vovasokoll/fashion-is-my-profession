from g4f.client import Client
import sys

client = Client()
msg = sys.argv[1]
response = client.chat.completions.create(
    model="gpt-3.5-turbo",
    messages=[{"role": "user", "content": msg}],
)
print(response.choices[0].message.content)
