#include "person_data_form_window.h"

#include "app/person_data_form/types/BodyData.h"
#include "app/person_data_form/types/GeneralData.h"
#include "app/person_data_form/types/PersonData.h"
#include "app/person_data_form/ui_person_data_form.h"

PersonFormWindow::PersonFormWindow(QWidget* parent)
    : QMainWindow(parent), ui(new Ui::PersonFormWindow), q_double_validator_(0.0, -1.0, -1) {
    ui->setupUi(this);

    q_double_validator_.setNotation(QDoubleValidator::ScientificNotation);

    QLineEdit* lineEditsWithDoubleValidators[] = {
      ui->weightEdit,      ui->heightEdit,          ui->chestGirthEdit, ui->waistGirthEdit,
      ui->hipGirthEdit,    ui->bodyfatEdit,         ui->muscleEdit,     ui->proteinEdit,
      ui->visceralFatEdit, ui->basalMetabolismEdit, ui->bmiEdit};

    for (QLineEdit* edit : lineEditsWithDoubleValidators) {
        edit->setValidator(&q_double_validator_);
    }
}

PersonFormWindow::~PersonFormWindow() {
    delete ui;
}

void PersonFormWindow::on_pushButton_clicked() {
    /*BodyData bodyData = {
        ui->weightEdit->text().toDouble(),
        ui->heightEdit->text().toDouble(),
        ui->chestGirthEdit->text().toDouble(),
        ui->waistGirthEdit->text().toDouble(),
        ui->hipGirthEdit->text().toDouble(),
        ui->bodyfatEdit->text().toDouble(),
        ui->muscleEdit->text().toDouble(),
        ui->waterEdit->text().toDouble(),
        ui->proteinEdit->text().toDouble(),
        ui->visceralFatEdit->text().toDouble(),
        ui->basalMetabolismEdit->text().toDouble(),
        ui->bmiEdit->text().toDouble(),
        WeightType(ui->bodyTypeCombobox->currentIndex())
    };*/
}

void PersonFormWindow::on_nextPageButton_clicked() {
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex() + 1);
}

void PersonFormWindow::on_prevPageButton_clicked() {
    ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex() - 1);
}