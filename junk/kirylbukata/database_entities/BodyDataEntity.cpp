//
// Created by savchik on 28/05/24.
//

#include "BodyDataEntity.h"

BodyDataEntity::BodyDataEntity(const QSqlRecord &record) : AbstractEntity(record) {

    if (record.contains("weight")) {
        body_data_.weight = record.field("weight").value().toDouble();
    }
    if (record.contains("height")) {
        body_data_.height = record.field("height").value().toDouble();
    }
    if (record.contains("weight")) {
        body_data_.weight = record.field("weight").value().toDouble();
    }
    if (record.contains("chest_girth")) {
        body_data_.chest_girth = record.field("chest_girth").value().toDouble();
    }
    if (record.contains("waist_girth")) {
        body_data_.waist_girth = record.field("waist_girth").value().toDouble();
    }
    if (record.contains("hip_girth")) {
        body_data_.hip_girth = record.field("hip_girth").value().toDouble();
    }
    if (record.contains("muscle")) {
        body_data_.muscle = record.field("muscle").value().toDouble();
    }
    if (record.contains("water")) {
        body_data_.water = record.field("water").value().toDouble();
    }
    if (record.contains("protein")) {
        body_data_.protein = record.field("protein").value().toDouble();
    }
    if (record.contains("bone_mass")) {
        body_data_.bone_mass = record.field("bone_mass").value().toDouble();
    }
    if (record.contains("visceral_fat")) {
        body_data_.visceral_fat = record.field("visceral_fat").value().toDouble();
    }
    if (record.contains("basal_metabolism")) {
        body_data_.basal_metabolism = record.field("basal_metabolism").value().toDouble();
    }
    if (record.contains("bmi")) {
        body_data_.bmi = record.field("bmi").value().toDouble();
    }

}

BodyData &BodyDataEntity::GetBodyData() {
    return body_data_;
}
