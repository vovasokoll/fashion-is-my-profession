//
// Created by Pavel Kasila on 8.05.24.
//

#ifndef CREATIVE_ABSTRACT_REPOSITORY_H
#define CREATIVE_ABSTRACT_REPOSITORY_H

#include <QList>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlRecord>
#include <QString>
#include <QStringList>
#include <memory>
#include <stdexcept>

namespace outfit::core::database {
template <typename Entity>
class AbstractRepository {
    static QString tableName() {
        throw std::runtime_error("specify table name");
    }

    std::unique_ptr<QSqlQueryModel> getModel() {
        std::unique_ptr<QSqlQueryModel> model = std::make_unique<QSqlQueryModel>();
        model->setQuery(QString("select * from %1").arg(tableName()));
        return model;
    }

    QList<Entity> listAll() {
        QSqlQuery query;
        query.prepare(QString("select * from %1").arg(tableName()));
        if (!query.exec()) {
            throw std::runtime_error("could not fetch query");
        }
        QList<Entity> list;
        while (query.next()) {
            list.append(Entity(query.record()));
        }
        return list;
    }

    Entity getById(Entity::Identifier id) {
        QSqlQuery query;
        query.prepare(QString("select * from %1 where id = :id").arg(tableName()));
        query.bindValue(":id", id);
        if (!query.exec()) {
            throw std::runtime_error("could not fetch query");
        }
        if (!query.next()) {
            throw std::runtime_error("no result");
        }
        return Entity(query.record());
    }
};
}  // namespace outfit::core::database

#endif  // CREATIVE_ABSTRACT_REPOSITORY_H
