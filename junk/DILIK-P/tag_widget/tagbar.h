#ifndef TAGBAR_H
#define TAGBAR_H

#include "database.h"
#include "flowlayout.h"
#include <QWidget>

class TagBar : public QWidget
{
    Q_OBJECT
public:
    explicit TagBar(QWidget *parent, DataBase& db, int id);

private:
    FlowLayout *flowLayout;

    QString buttonStyle(int color);
};

#endif // TAGBAR_H
