#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication application(argc, argv);

    MainWindow sortWindow;
    sortWindow.setWindowTitle("Clothing Adviser");
    sortWindow.show();
    return application.exec();
}
